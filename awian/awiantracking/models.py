from django.db import models
from tracking.models import Visitor
# Create your models here.


class VisitorDetails(models.Model):
    class Meta:
        verbose_name_plural = "Visitor Details"
    visitor = models.ForeignKey(Visitor, null=True, blank=True, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    country = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "# {} - {}".format(self.id, self.country)
