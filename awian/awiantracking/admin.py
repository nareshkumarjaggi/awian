from django.contrib import admin
from awiantracking.models import VisitorDetails
from tracking.models import Visitor
from tracking.admin import VisitorAdmin


class VisitorDetailsAdmin(admin.ModelAdmin):
    fields = ['country']

admin.site.register(VisitorDetails, VisitorDetailsAdmin)
