from django.http import HttpResponse
from awiantracking.models import VisitorDetails


class CreateCountryMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        VisitorDetails.objects.create(country=request.geo_data.country_name)
        return self.get_response(request)

    def process_exception(self, request, exception):
        print(exception)
        return HttpResponse("in exception")