from django.shortcuts import render
from django.views import View
from tracking.utils import get_ip_address
from ipware import get_client_ip


class DashBoard(View):
    template_name = "dashboard/home.html"

    def get(self, request):
        print(request.geo_data.country_name)
        return render(request, template_name=self.template_name, context={})
